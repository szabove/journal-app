import { plainToClass, plainToInstance } from 'class-transformer';
import { validate } from 'class-validator';
import { Request, Response, NextFunction } from 'express';
import handleError, { ErrorHandler } from '../utils/handleError';

const validateBody = (body: any) => {
    return async (
        req: Request,
        res: Response,
        next: NextFunction
    ): Promise<Response | void> => {
        console.log(body);

        const mapped = plainToInstance(body, req.body);
        const errors = await validate(mapped);
        if (errors && errors.length > 0) {
            return handleError(
                res,
                new ErrorHandler(400, 'Model validation failed.', {})
            );
        } else {
            return next();
        }
    };
};

export default validateBody;
