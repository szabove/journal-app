import { Response } from "express"
import handleError, { ErrorHandler } from "../utils/handleError"

export default class BaseController 
{
    protected Ok = (res: Response, data?: any): Response => 
    {
        return res.status(200).json(data);
    }

    protected Unauthorized = (res: Response, message?: string): Response => 
    {
        return handleError(res, new ErrorHandler(401, message || 'User is not authorized for this action.'));
    }

    protected Forbidden = (res: Response, message?: string): Response => 
    {
        return handleError(res, new ErrorHandler(403, message || 'User is not permitted for this action.')); 
    }

    protected NotFound = (res: Response, message?: string): Response => 
    {
        return handleError(res, new ErrorHandler(404, message || 'Resource not found.'));
    }

    protected Error = (res: Response, message?: string): Response => 
    {
        return handleError(res, new ErrorHandler(500, message || 'Server error.'));
    }
}