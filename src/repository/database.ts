import { Sequelize } from "sequelize";
//import { DB_NAME, DB_PASSWORD, DB_USER } from "../config";

const database = new Sequelize({
    dialect: 'postgres',
    database: 'GrowJournalApp',
    username: 'postgres',
    password: 'postgresql'
});

//database.sync();

export default database;